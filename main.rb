require 'sinatra/base'
require 'haml'

class MainWebsite < Sinatra::Application
  set :bind, '0.0.0.0' if development?

  get '/' do
    haml :index
  end
end
