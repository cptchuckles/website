require './main.rb'
require './hangman/hangman.rb'
require './filestore/filestore.rb'

DB = Sequel.connect(ENV['DATABASE_URL'] || "sqlite://db/devdb")

map '/' do
  run MainWebsite.new
end

map '/hangman' do
  run Hangman.new
end

map '/filestore' do
  run Filestore.new
end
